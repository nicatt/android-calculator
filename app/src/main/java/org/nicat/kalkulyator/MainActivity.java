package org.nicat.kalkulyator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView txtScreen, txtHistory;
    Button btnDelete, btnErase, btnEquals;

    boolean isNumber = false;
    int result = 0;
    String last;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    public void init(){
        txtScreen = findViewById(R.id.txtScreen);
        txtHistory = findViewById(R.id.txtHistory);

        btnDelete = findViewById(R.id.btnDelete);
        btnErase  = findViewById(R.id.btnErase);
        btnEquals = findViewById(R.id.btnEquals);


        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str = (String) txtScreen.getText();
                if(str.length() > 1){
                    str = str.substring(0, str.length() - 1);
                }
                else{
                    str = "0";
                }
                txtScreen.setText(str);
            }
        });

        btnEquals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer screen = Integer.parseInt((String)txtScreen.getText());
                String history = (String)txtHistory.getText();

                switch (last){
                    case "+":
                        txtHistory.setText(history + screen);
                        result = result + screen;
                        break;
                    case "*":
                        txtHistory.setText(history + screen);
                        result = result * screen;
                        break;
                }

                txtScreen.setText(String.valueOf(result));
            }
        });

        btnErase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtScreen.setText("0");
                txtHistory.setText("");
                result = 0;
                last = null;
            }
        });


    }

    public void operationClick(View v){
        String current = (String) txtHistory.getText();
        String screen  = (String) txtScreen.getText();

        switch (v.getId()){
            case R.id.btnPlus:
                last = "+";
                result = result + Integer.parseInt(screen);
                break;
            case R.id.btnTimes:
                last = "*";
                result = result * Integer.parseInt(screen);
                break;
        }


        txtHistory.setText(current + txtScreen.getText() + last);
        txtScreen.setText("0");
    }

    public void numberClick(View view){
        switch (view.getId()){
           case R.id.btnNum0:
               editNumber(0);
               break;
           case R.id.btnNum1:
               editNumber(1);
               break;
           case R.id.btnNum2:
               editNumber(2);
               break;
           case R.id.btnNum3:
               editNumber(3);
               break;
           case R.id.btnNum4:
               editNumber(4);
               break;
           case R.id.btnNum5:
               editNumber(5);
               break;
           case R.id.btnNum6:
               editNumber(6);
               break;
           case R.id.btnNum7:
               editNumber(7);
               break;
           case R.id.btnNum8:
               editNumber(8);
               break;
           case R.id.btnNum9:
               editNumber(9);
               break;
        }
    }

    public void editNumber(int number){
        String current = (String) txtScreen.getText();
        String newNumber;

        if(current.equals("0")){
            newNumber = String.valueOf(number);
        }
        else{
            newNumber = txtScreen.getText() + String.valueOf(number);
        }

        txtScreen.setText(newNumber);
    }
}
